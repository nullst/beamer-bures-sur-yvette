# Bures-sur-Yvette beamer theme

* Cleaner slide designs: no headlines or footers
* Bright green and yellow color scheme
* Unusual blocks: not colored, highlighted on the left margin
* Typeset using `Noto Sans` font family (XeTeX is required)
* Flexible frame title font size: `\Large` or `\large` depending on the title length.

Beware: only some beamer features are realized. Also,  most blocks are correctly typeset only after a second compilation.

## Usage

Put the style file in the directory. Use `\usetheme{Bures-sur-Yvette}` in the `tex`-file.

## Known issues

### The command `\pause` does not work well inside blocks

The `\pause` beamer command works by telling `PGF` to make a part of
the slide invisible. The blocks in this theme are drawn using TikZ,
which itself relies on PGF, so the block decorations also sometimes
disappear.

**Workaround**: make sure that commands like `\end{theorem}` are always in the
visible part of the slide, using for instance `\onslide` as in the following example:
```
\begin{frame}
  \begin{theorem}
    First part
  
    \pause

    Second part
    \onslide<1->
  \end{theorem}
  \onslide<2->

  Some text only shown on the second slide..
\end{frame}
```

Alternatively, consider the more flexible overlay specification
commands, like `\only` and `\alt`.


